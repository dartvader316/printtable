#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "printtable.h"


int main(){
    srand(time(0));

    const size_t table_height = 40;
    const size_t table_width = 6;

    char* **table = malloc(sizeof(char**) * table_height);

    static char* header[] = {"index","column1", "column2", "column3","column4","hello", NULL};
    table[0] = header; // First row is a header


    for(size_t h = 1;h<table_height;h++){
        // Allocate row
        table[h] = malloc(sizeof(char*) * table_width);

        for(size_t w = 0;w<table_width;w++){
            char* c = table[h][w];
            // Allocate cell. It is just a string ( char* )
            table[h][w] = malloc(sizeof(char) * 128);
            table[h][w][0] = '\0';

            if(w == 0){
                // Index column cells fill
                sprintf(table[h][w], "%ld", h);
            }else{
                // Other cells with random data
                if(rand()%100==1){
                    sprintf(table[h][w], "%s", "hello table world!");             
                }else{
                    static const int rand_max = 1000000000;
                    sprintf(table[h][w],"%d",(rand()%rand_max)-(rand_max/2));
                }
            }
        }
    }
    
    FILE* f = fopen("table.txt","w+");
    int printedf = fprinttable(f,(const char***)table, table_height,table_width);
    fclose(f);

    int printed = printtable((const char***)table,10,table_width);
    printf("Printed %d characters \n", printed);
    
}