#include <stdio.h>
#include <string.h>
#include <errno.h>

/**
 * @brief Print table to stdout
 * 
 * @param table_data Table data. 2d array of char*. First row will be table header
 * @param height Table height
 * @param width Table width
 * @return int Amount of printed characters
 */
int printtable(const char* **table_data, size_t height, size_t width);

/**
 * @brief Print table 
 *
 * @param out output file descriptor
 * @param table_data Table data. 2d array of char*. First row will be table heade
 * @param height Table height
 * @param width Table width
 * @return int Amount of printed characters
 */
int fprinttable(FILE* out,const char* **table_data, size_t height, size_t width);


#define _PRINTTABLE_MAX_COLUMNS 64 // Good luck for using more than 64 columns

static const char _vline = '|';
static const char _hvsep = '+';
static const char _hline = '-';

static inline int _fprinttable_separator_row(FILE* out,size_t width, size_t* max_widths){
    int ret = 0;
    ret += fprintf(out,"%c",_hvsep);

    for(size_t w = 0;w<width;w++){
        for(size_t i = 0;i<max_widths[w]+2;i++){
            ret+=fprintf(out,"%c",_hline);
        }
        ret += fprintf(out,"%c",_hvsep);
    }

    ret += fprintf(out, "\n");

    return ret;
}

static inline int _fprinttable_data_row(FILE *out,const char* **table_data, size_t width, size_t h, size_t *max_widths){
    int ret = 0;
    ret += fprintf(out,"%c",_vline);

    for(size_t w = 0;w<width;w++){
        ret += fprintf(out, " %*s ",(int)max_widths[w],table_data[h][w]);
        ret += fprintf(out,"%c",_vline);
    }

    ret +=fprintf(out, "\n");

    return ret;
}

int printtable(const char* **table_data, size_t height, size_t width){
    return fprinttable(stdout,table_data,height,width);
}

int fprinttable(FILE* out,const char* **table_data, size_t height, size_t width){
    size_t max_widths[_PRINTTABLE_MAX_COLUMNS]; 

    if(width > _PRINTTABLE_MAX_COLUMNS){
        errno = EOVERFLOW;
        return -1;
    }
    memset(max_widths,0,width*sizeof(size_t));

    // Calculate max columns width

    for(size_t w = 0;w<width;w++){
        for(size_t h = 0;h<height;h++){
            const char* s = table_data[h][w];
            const size_t len = strlen(s);

            if(len > max_widths[w]){
                max_widths[w] = len;
            }
        }
    }
    int ret = 0;

    // Print Header
    ret += _fprinttable_separator_row(out, width, max_widths);
    ret += _fprinttable_data_row(out, table_data, width, 0, max_widths);
    ret += _fprinttable_separator_row(out, width, max_widths);
    
    // Print Data
    for(size_t h = 1;h<height;h++){
        ret += _fprinttable_data_row(out, table_data, width, h, max_widths);
    }
    
    ret += _fprinttable_separator_row(out, width, max_widths);

    return ret;
}
